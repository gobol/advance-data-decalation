       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing12-3.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  InputString    PIC X(8) .

       01  WorkArea.
           02 Fnum   PIC 9(5) VALUE ZEROS .
           02 Snum   PIC  99  VALUE ZEROS .

       01  WorkNum  REDEFINES WorkArea PIC 99999V99.

       01  EditedNum   PIC ZZ,ZZ9.99.

       PROCEDURE DIVISION .
       Begin.
           DISPLAY "Enter a decimal number - " WITH NO ADVANCING 
           ACCEPT InputString 
           UNSTRING InputString DELIMITED BY ".", ALL SPACES
              INTO Fnum, Snum  
           MOVE  WorkNum  TO EditedNum
           DISPLAY "Decimal Number = " EditedNum 
           ADD 10 TO WorkNum 
           MOVE  WorkNum  TO EditedNum 
           DISPLAY "Decimal Number = " EditedNum 
           . 
       
