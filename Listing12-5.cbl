       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing12-5.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  NanoSecs    PIC 9(10).
       01  MicroSecs   REDEFINES NanoSecs PIC 9999999V999.
       01  MilliSecs   REDEFINES NanoSecs PIC 9999V999999.
       01  Seconds     REDEFINES NanoSecs PIC 9V999999999.

       01  EditedNum   PIC Z,ZZZ,ZZZ,ZZ9.99.

       PROCEDURE DIVISION .
       Begin.
           MOVE  1234567895  TO NanoSecs
           MOVE NanoSecs TO EditedNum 
           DISPLAY EditedNum " NanoSecs"

           MOVE  MicroSecs TO EditedNum 
           DISPLAY EditedNum " MicroSecs"

           MOVE MilliSecs TO EditedNum 
           DISPLAY EditedNum " MilloSecs"

           MOVE Seconds  TO EditedNum 
           DISPLAY EditedNum " Seconds"
           STOP RUN.
